document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('.nav-bar a.button.responsive-button').addEventListener('click', () => {
        document.querySelector('nav .nav-bar').classList.add('nav-bar-popup');
    });
    
    document.querySelector('nav a.nav-button-popup-close').addEventListener('click', () => {
        document.querySelector('nav .nav-bar').classList.remove('nav-bar-popup');
    });
});